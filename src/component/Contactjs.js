import React, { Component } from 'react';
import { BrowserRouter as Router,  Redirect} from "react-router-dom";

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state= {
      isRedirect : false,
      fname:"",
      fmess:"",
      fphone:"", //có thể k khai báo, react tự hiểu
      femail:""
    }
    
  }
  
  submitForm = (event) =>{
    event.preventDefault()
    this.setState({
      isRedirect: true
    })
  }
    isChange =(event) => {
      const ten = event.target.name;
      const giatri = event.target.value;
     this.setState({
       [ten] : giatri})  
  }
  isFile =(event) => {
    const anh = event.target.files[0].name
    console.log(anh)
    
    // this.setState({
    //   fanh: anh1
    // })
  }
   showValue=()=> {
    var noidung = "";
     noidung += "ten: " + this.state.fname;
     noidung += "gmail: " + this.state.femail;
     noidung +="dienthoai: "+ this.state.fphone;
     noidung +="message: " + this.state.fmess;
     noidung +="Ngay: " + this.state.fngay;
    //  noidung +="File: " + this.state.fanh;
     return noidung
   }
  
    render() {
      if(this.state.isRedirect){
        console.log(this.showValue())
        return (
        <Redirect to="/"/> )
        
        
      }

        return (
          
            <div>
        <header className="masthead">
          <div className="container h-100">
            <div className="row h-100">
              <div className="col-lg-7 my-auto">
                <div className="header-content mx-auto">
                  <h1 className="mb-5">Trang lien he</h1>
                </div>
              </div>
            </div>
          </div>
        </header>
        {/*begin contact*/}
        <div className="container">
          {/* Contact Section Heading*/}
          <h2 className="page-section-heading text-center text-uppercase text-secondary mb-0">Contact Me</h2>
          {/* Icon Divider*/}
          <div className="divider-custom">
            <div className="divider-custom-line" />
            <div className="divider-custom-line" />
          </div>
          {/* Contact Section Form*/}
          <div className="row">
            <div className="col-lg-8 mx-auto">
              {/* To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19.*/}
              <form id="contactForm" name="sentMessage" noValidate="novalidate">
                <div className="control-group">
                  <div className="form-group floating-label-form-group controls mb-0 pb-2">
                    <label>Name</label>
                    <input name="fname" onChange={(event)=>this.isChange(event)} className="form-control" id="name" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name." />
                    <p className="help-block text-danger" />
                  </div>
                </div>
                <div className="control-group">
                  <div className="form-group floating-label-form-group controls mb-0 pb-2">
                    <label>Email Address</label>
                    <input name="femail" onChange={(event)=>this.isChange(event)} className="form-control" id="email" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address." />
                    <p className="help-block text-danger" />
                  </div>
                </div>
                <div className="control-group">
                  <div className="form-group floating-label-form-group controls mb-0 pb-2">
                    <label>Phone Number</label>
                    <input name="fphone" onChange={(event)=>this.isChange(event)} className="form-control" id="phone" type="tel" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number." />
                    <p className="help-block text-danger" />
                  </div>
                </div>
                <div className="control-group">
                  <div className="form-group floating-label-form-group controls mb-0 pb-2">
                    <label>Message</label>
                    <textarea name="fmess" onChange={(event)=>this.isChange(event)} className="form-control" id="message" rows={5} placeholder="Message" required="required" data-validation-required-message="Please enter a message." defaultValue={""} />
                    <p className="help-block text-danger" />
                  </div>
                </div>
                <div className="control-group">
                  <div className="form-group floating-label-form-group controls mb-0 pb-2">
                    <label>Ngày</label>
                    <select name="fngay"  className="form-control"onChange={(event)=>this.isChange(event)}>
                      <option name="Thu3"> Thu 3 </option>
                      <option name="Thu5"> Thu 5 </option>
                      <option name="Thu7"> Thu 7 </option>
                      <option name="Chunhat"> Chu nhat </option>
                    </select>
                  </div>
                </div>
                <br />
                <div className="control-group">
                  <div className="form-group floating-label-form-group controls mb-0 pb-2">
                    <label>Chọn file</label>
                    <input type="file" className="form-control-file" name="file" onChange={(event)=>this.isFile(event)}/>
                  </div>
                </div>
                <br />
                <div id="success" />
                <div className="form-group"><button className="btn btn-primary btn-xl" id="sendMessageButton" type="submit" onClick={(event)=>this.submitForm(event)}>Send</button></div>
              </form>
            </div>
          </div>
        </div>
      </div>
        );
    }
}

export default Contact;