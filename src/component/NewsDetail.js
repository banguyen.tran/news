import React, { Component } from 'react';
import {
  BrowserRouter as Router,

  useParams
} from "react-router-dom";
import Dl from "../duLieu.json"
import NewsRalative from './NewsRalative';

function NewsDetail() {
  var params = useParams();
  var dem = 1;
  return (
    <div>
      <header className="masthead">
        <div className="container h-100">
          <div className="row h-100">
            <div className="col-lg-7 my-auto">
              <div className="header-content mx-auto">
                <h1 className="mb-5 text-center">Trang tin chi tiet</h1>
              </div>
            </div>
          </div>
        </div>
      </header>

      {Dl.map((value, key) => {
        if (value.id == params.id) {
          return (
            <div className="jumbotron jumbotron-fluid" key={key}>
              <div className="container">
                <img src={value.anh} className="img-fluid" alt="" />
                <h3 className="card-title">{value.tieude}</h3>
                <p>{value.trichdam}</p>
                <hr className="my-2" />
                <p>{value.noidung}</p>
              </div>
            </div>)
        }
      })
      }

      <div className="container">
        <div className="row -mt3">
          { Dl.map((value,key) =>{
            if(value.id!= params.id) {
              if(dem<=4){
                dem++
                return(
                  <NewsRalative key={key} id={value.id}
                  Src={value.anh}
                  Tieude={value.tieude}
                  Trichdoan={value.trichdam}
                  />
                )
              }
              
            }
          })}
      </div>
      </div>

    </div>)
}
export default NewsDetail;