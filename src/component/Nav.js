import React, { Component } from 'react';
import {BrowserRouter as Link, NavLink } from "react-router-dom";
class Nav extends Component {
  constructor(props) {
    super(props);
    
  }
  
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div className="container">
        <NavLink to="/">HOME</NavLink> {// co the dung link hoac NavLink
        }
          <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
                <i className="fas fa-bars" />
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav ml-auto">
              <li>
                <NavLink to="/tin">Tin tuc</NavLink>
              </li>
              {/* <li>
                <NavLink to="/tinchitiet">Tin chi tiet</NavLink>
              </li> */}
              <li>
                <NavLink to="/lienhe">Contact</NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Nav;