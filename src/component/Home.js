import React, { Component } from 'react';

class Home extends Component {
    render() {
        return (
            <div>
            <header className="masthead">
            <div className="container h-100">
              <div className="row h-100">
                <div className="col-lg-7 my-auto">
                  <div className="header-content mx-auto">
                    <h1 className="mb-5">Trang tin tuc</h1>
                    <a href="/tin" className="btn btn-outline btn-xl js-scroll-trigger">Start Now for Free!</a>
                  </div>
                </div>
                <div className="col-lg-5 my-auto">
                  <div className="device-container">
                    <div className="device-mockup iphone6_plus portrait white">
                      <div className="device">
                        <div className="screen">
                          <img src="https://tutamlangblog.files.wordpress.com/2020/02/1124114-800w.jpg" className="img-fluid" alt="" />
                        </div>
                        <div className="button">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </header>
          <section className="download bg-primary text-center" id="download">
            <div className="container">
              <div className="row">
                <div className="col-md-8 mx-auto">
                  <h2 className="section-heading">Simple Magazine</h2>
                </div>
              </div>
            </div>
          </section>
        </div>
        );
    }
}

export default Home;