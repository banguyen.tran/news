import React, { Component } from 'react';
import Dl from "../duLieu.json"
import NewsItem from './NewsItem';

class News extends Component {
  constructor(props) {
    super(props);
    
  }
  
  render() {
    return (
      <div>

        <header className="masthead">
          <div className="container h-100">
            <div className="row h-100">
              <div className="col-lg-7 my-auto">
                <div className="header-content mx-auto">
                  <h1 className="mb-5">Danh sach tin</h1>
                </div>
              </div>
            </div>
          </div>
        </header>

        <div className="container">
          <div className="row -mt3">
          {Dl.map((value,key)=>{
            return (
              <NewsItem key={key}
              id={value.id}
              Anh={value.anh}
              Tieude={value.tieude}
              Trichdan={value.trichdam}
              />
            )
          }
          )}
          </div>
        </div>
      </div>
    );
  }
}

export default News;