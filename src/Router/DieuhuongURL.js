import React, { Component } from 'react';
import {BrowserRouter as  Switch,Route,Link,NavLink } from "react-router-dom";
import Contact from '../component/Contactjs';
import NewsDetail from '../component/NewsDetail';
import Home from './../component/Home'
import News from './../component/News';
class DieuhuongURL extends Component {
    render() {
        return (
            <div>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route exact path="/tin">
                    <News />
                </Route>
                <Route exact path="/tinchitiet/:slug.:id.html">
                    <NewsDetail />
                </Route>
                <Route exact path="/lienhe">
                    <Contact />
                </Route>
             
                </div>
        );
    }
}

export default DieuhuongURL;