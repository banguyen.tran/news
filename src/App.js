import './App.css';
import Nav from './component/Nav'
import Footer from './component/Footer'
import { BrowserRouter as Router} from "react-router-dom";
import DieuhuongURL from './Router/DieuhuongURL'
function App() {
  return (

    <Router>
      <Nav></Nav>
      <DieuhuongURL />
      <Footer />
    </Router>

  );
}

export default App;
